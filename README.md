# itunes-search.py
Search the iTunes store to find the bundleID and version number of iOS applications for use with MDM tools.

### Installation

This tool uses a couple of non-standard Python modules that are available via `pip`

```
pip install requests
pip install terminaltables
```

---

If you don't have `pip`, try using `easy_install`:

`easy_install pip`

Then:

```
pip install requests
pip install terminaltables
```

More info about the [requests](http://docs.python-requests.org/en/master/) module and the [terminaltables](https://pypi.python.org/pypi/terminaltables) module.

### Usage

`python itunes-search.py Netflix`

Should show results similar to:

```
Results: 10
+----+--------------------------------------------------------------+---------+-------------------------------+----------------------------+
| #  | App Name                                                     | Version | bundleID                      | vppDeviceAssignmentEnabled |
+----+--------------------------------------------------------------+---------+-------------------------------+----------------------------+
| 1  | Netflix                                                      | 8.25.1  | com.netflix.Netflix           | True                       |
| 2  | iMovie                                                       | 2.2.3   | com.apple.iMovie              | True                       |
| 3  | BuddyTV Guide for Netflix, HBO GO, Amazon, Hulu, Crackle and | 3.4.5   | com.buddytv.guide             | True                       |
|    | More: Movie, TV Listings with Remote Control                 |         |                               |                            |
| 4  | Upflix for Netflix                                           | 3.1.6   | com.douglas.upflix            | True                       |
| 5  | NextGuide Remote with Free TV Guide, Netflix Queue           | 3.3.11  | com.myumee.app.UMEE           | True                       |
|    | Management and Roku Wifi Control                             |         |                               |                            |
| 6  | IMDb Movies & TV                                             | 7.3     | com.imdb.imdb                 | True                       |
| 7  | Fandango Movies - Times + Tickets                            | 7.9     | com.fandango.fandango         | True                       |
| 8  | Movies by Flixster, with Rotten Tomatoes                     | 7.13.2  | com.jeffreygrossman.moviesapp | True                       |
| 9  | Crackle - Movies & TV                                        | 5.3     | com.crackle.crackle-iphone    | True                       |
| 10 | Action Movie FX                                              | 3.2.8   | com.badrobot.actionmoviefx    | True                       |
+----+--------------------------------------------------------------+---------+-------------------------------+----------------------------+
```

#### Results
The results displayed include:

* `#` The item number in the list
* `App Name` The App Name
* `Version` The Current Version of the App
* `bundleID` The BundleID
* `vppDeviceAssignmentEnabled` If VPP Device Based Assignment is enabled for the app
 


#### Flags

Current options include the `--count` flag to increase the results displayed up to 50.