#!/usr/bin/python

import requests
import json
import argparse
from textwrap import wrap
from terminaltables import AsciiTable

############################## ARG PARSE ##############################

parser = argparse.ArgumentParser(description='Search the iTunes store')
parser.add_argument('--count', help='Number of results to return. Max of 50')
parser.add_argument('search', help='Search term')

args = parser.parse_args()
app_name = args.search
if args.count:
    
    if int(args.count) > 50:
        count = '50'
    elif int(args.count) <= 0:
        count = '20';
    else:
        count = '{}'.format(args.count)

else:
    count = '10'

######################################################################

url = 'https://itunes.apple.com/search?country=US&media=software&entity=iPadSoftware,software&term='

request = requests.get(url + app_name + '&limit=' + count)

if request.status_code == 200:
    data = json.loads(request.text)

    numb_results = data['resultCount']
    print 'Results: {}'.format(numb_results)
    # app_list = [['#','App Name', 'Version', 'bundleID', 'URL']]
    app_list = [['#','App Name', 'Version', 'bundleID', 'vppDeviceAssignmentEnabled']]
    
    i = 1;
    for item in data['results']:
        # app = [i, item['trackName'], item['version'], item['bundleId'], item['trackViewUrl']]
        app = [i, item['trackName'], item['version'], item['bundleId'], item['isVppDeviceBasedLicensingEnabled']]
        
        app_list.append(app)
        i = i + 1
        
    for item in app_list:
        if len(item[1]) > 20:
            wrapped_string = '\n'.join(wrap(item[1], 60))
            item[1] = wrapped_string
            
        # if len(item[4]) > 20:
        #     wrapped_string = '\n'.join(wrap(item[4], 60))
        #     item[4] = wrapped_string
            
    table = AsciiTable(app_list)
    print table.table
    
else:

    print "Error {}:\n{}".format(request.status_code, request.text)
